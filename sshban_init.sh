#! /bin/sh
### BEGIN INIT INFO
# Provides:          sshban
# Required-Start:    mountkernfs
# Required-Stop:
# Should-Start:
# X-Start-Before:
# Default-Start:     S
# Default-Stop:
# Short-Description: Read sshd logs and ban abusers
# Description: Read sshd logs and ban abusers
### END INIT INFO

if [ true != "$INIT_D_SCRIPT_SOURCED" ] ; then
    set "$0" "$@"
    INIT_D_SCRIPT_SOURCED=true . /lib/init/init-d-script
fi

DESC="SSHban"
DAEMON=/usr/local/bin/sshban
PIDFILE=/run/sshban.pid
USER=sshban
GROUP=nogroup
#ARGS="-d"
ARGS=""
SOURCE_FIFO=/run/rsyslog_sshd.fifo
ACTION="sudo /etc/firewall/ban.py"

do_start_cmd() {
    if [ ! -p ${SOURCE_FIFO} ]; then
        mkfifo -m 600 ${SOURCE_FIFO}
        chown ${USER} ${SOURCE_FIFO}
    fi

    start-stop-daemon --start --oknodo --pidfile ${PIDFILE} --user ${USER} \
            --background --make-pidfile --chuid ${USER}:${GROUP} \
            --startas ${DAEMON} -- ${ARGS} ${SOURCE_FIFO} "${ACTION}"
    return $?
}

do_stop() {
    start-stop-daemon --stop --oknodo --pidfile ${PIDFILE} --user ${USER}
    return $?
}
do_status() {
    status_of_proc -p ${PIDFILE} ${DAEMON} ${DESC}
    return $?
}
